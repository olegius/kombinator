import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-number-list',
  styles: ['app-number:not(:last-child):after { content: "-" }'],
  template: `
    <app-number [value]=number *ngFor="let number of numbers"></app-number>
  `
})

export class NumberListComponent {
  @Input() numbers: number[];
}
