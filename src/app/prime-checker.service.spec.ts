import { TestBed, inject } from '@angular/core/testing';

import { PrimeCheckerService } from './prime-checker.service';

describe('PrimeCheckerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PrimeCheckerService]
    });
  });

  it('should be created', inject([PrimeCheckerService], (service: PrimeCheckerService) => {
    expect(service).toBeTruthy();
  }));

  it('should be prime', inject([PrimeCheckerService], (service: PrimeCheckerService) => {
    expect(service.isPrime(2)).toBeTruthy();
    expect(service.isPrime(3)).toBeTruthy();
    expect(service.isPrime(5)).toBeTruthy();
  }));

  it('should not be prime', inject([PrimeCheckerService], (service: PrimeCheckerService) => {
    expect(service.isPrime(1)).toBeFalsy();
    expect(service.isPrime(6)).toBeFalsy();
    expect(service.isPrime(9)).toBeFalsy();
  }));
});
