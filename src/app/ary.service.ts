import { Injectable } from '@angular/core';

@Injectable()
export class AryService {
  constructor() {}
  public ary (n=0) {
    return Array(n).fill(0).map(_ => {
      return Math.floor(Math.random() * 1000)
    });
  }
}
