import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {AryService} from './ary.service';
import {MergeSortedArrayService} from './merge-sorted-array.service';
import {PrimeCheckerService} from './prime-checker.service';

import { AppComponent } from './app.component';
import { NumberComponent } from './number.component';
import { NumberListComponent } from './number-list.component';


@NgModule({
  declarations: [
    AppComponent,
    NumberComponent,
    NumberListComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    AryService,
    MergeSortedArrayService,
    PrimeCheckerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
