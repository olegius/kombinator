import { Injectable } from '@angular/core';

@Injectable()
export class MergeSortedArrayService {

  constructor() { }

  public merge_and_sort(ary1 = [], ary2 = []): number[] {
    return ary1.concat(ary2).sort(this.sorter);
  }

  private sorter(a: number, b: number): number {
    if (a > b) { return 1; }
    if (a < b) { return -1; }
    return 0;
  }
}
