import { TestBed, inject } from '@angular/core/testing';

import { AryService } from './ary.service';

describe('AryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AryService]
    });
  });

  it('should be created', inject([AryService], (service: AryService) => {
    expect(service).toBeTruthy();
  }));
});
