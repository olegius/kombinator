import { Component, OnInit, Input } from '@angular/core';
import { PrimeCheckerService } from './prime-checker.service';

@Component({
  selector: 'app-number',
  template: '<span [class.prime]="isPrime">{{value}}</span>',
  styles: ['.prime { background-color: lime; }']
})

export class NumberComponent implements OnInit {
  public isPrime: boolean;

  @Input() value: number;

  constructor(private primeChecker: PrimeCheckerService) {}

  ngOnInit() {
    this.isPrime = this.primeChecker.isPrime(this.value);
  }
}
