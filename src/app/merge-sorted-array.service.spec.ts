import { TestBed, inject } from '@angular/core/testing';

import { MergeSortedArrayService } from './merge-sorted-array.service';

describe('MergeSortedArrayService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MergeSortedArrayService]
    });
  });

  it('should be created', inject([MergeSortedArrayService], (service: MergeSortedArrayService) => {
    expect(service).toBeTruthy();
  }));

  it('should merge two arrays', inject([MergeSortedArrayService], (service: MergeSortedArrayService) => {
    const ary1 = [6, 2, 9];
    const ary2 = [3, 1, 8];
    const expected = [1, 2, 3, 6, 8, 9];
    expect(service.merge_and_sort(ary1, ary2)).toEqual(expected);
  }));
});
