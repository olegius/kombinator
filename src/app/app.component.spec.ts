import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { NumberComponent } from './number.component';
import { NumberListComponent } from './number-list.component';
import {AryService} from './ary.service';
import {MergeSortedArrayService} from './merge-sorted-array.service';
import { PrimeCheckerService } from './prime-checker.service';



describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        NumberComponent,
        NumberListComponent
      ],
      providers: [
        AryService,
        MergeSortedArrayService,
        PrimeCheckerService
      ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
