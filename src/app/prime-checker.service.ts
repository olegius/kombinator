import { Injectable } from '@angular/core';

@Injectable()
export class PrimeCheckerService {

  constructor() {}

  public isPrime(num: number): boolean {
    for (let i = 2, s = Math.sqrt(num); i <= s; i++) {
      if (num % i === 0) {
        return false;
      }
    }
    return num !== 1;
  }
}
