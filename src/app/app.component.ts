import { Component } from '@angular/core';
import {AryService} from './ary.service';
import {MergeSortedArrayService} from './merge-sorted-array.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private aryService: AryService, private mergeSortedArrayService: MergeSortedArrayService) {}
  public arys: number[][] = [
    this.aryService.ary(10),
    this.aryService.ary(10),
  ];

  public result: number[] =
    this.mergeSortedArrayService.merge_and_sort(this.arys[0], this.arys[1]);

  public regen () {
    this.arys = [
      this.aryService.ary(10),
      this.aryService.ary(10)
    ];

    this.result =
      this.mergeSortedArrayService.merge_and_sort(this.arys[0], this.arys[1]);
  }
}
